# Robot Quarantine

Quarantine for Robot Framework tests

## Installation

- Sample `docker run`:

```
$ docker run \
	--network mongo_default \
	-e MONGO_USER=root \
	-e MONGO_PASSWORD=example \
	-e MONGO_HOST=mongo:27017 \
	-p 8080:8080 \
	registry.gitlab.com/topi.kettunen/robot-quarantine
```

- Sample stack:

```
---
version: '3.4'

services:
  quarantine:
    image: registry.gitlab.com/topi.kettunen/robot-quarantine
	ports:
	  - "8080:8080"
	environment:
	  - MONGO_USER=root
	  - MONGO_PASSWORD=example
	  - MONGO_HOST=mongo:27017
	networks:
	  - default
	  - mongo_default
	  
networks:
  default:
    attachable: true
  mongo_default:
    external: true
```

## Sample Requests

- `POST`

```
$ curl -X POST http://localhost:8080/uuids \
	-H "Content-Type: application/json" \
	-d '{
		"uuid": "e5c5c6d0-0189-4234-89b6-29c6a1a50a03"
	}'
```

- `DELETE`

```
$ curl -X DELETE http://localhost:8080/uuids \
	-H "Content-Type: application/json" \
	-d '{
		"uuid": "e5c5c6d0-0189-4234-89b6-29c6a1a50a03"
	}'
```


